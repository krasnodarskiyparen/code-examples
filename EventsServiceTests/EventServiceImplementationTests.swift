//
//  EventsListServiceImplementationTests.swift
//  picmeTests
//
//  Created by Kirill Makarenko on 02.11.17.
//  Copyright © 2017 S Media Link. All rights reserved.
//

import XCTest
import Nimble
@testable import picme

class EventServiceImplementationTests: XCTestCase {

    var service: EventServiceImp!
    var operationFactoryMock: EventOperationFactoryMock!
    var schedulerMock: OperationSchedulerMock!

    override func setUp() {
        super.setUp()

        operationFactoryMock = EventOperationFactoryMock()
        schedulerMock = OperationSchedulerMock()

        service = EventServiceImp(eventOperationFactory: operationFactoryMock, operationScheduler: schedulerMock)
    }

    override func tearDown() {
        super.tearDown()

        service = nil
        operationFactoryMock = nil
        schedulerMock = nil
    }

    func testThatServiceAddsOperationInScheduler() {
        // given
        let event = EventPlainObject(id: 0, name: "", location: "", startDate: Date())

        // when
        service.obtainUpcomingEvents(event: event, paginationDirection: .next, successBlock: { _ in }, errorBlock: { _ in })

        // then
        expect(self.operationFactoryMock.invokedGetUpcomingEventsOperationCount) == 1
        expect(self.schedulerMock.addCalled) == true
    }

    func testThatServiceReturnsSuccessBlockOnObtainUpcomingEventsForPagination() {
        // given
        let expectation = self.expectation(description: "Expect that service obtains upcoming events")
        let event = EventPlainObject(id: 0, name: "", location: "", startDate: Date())

        let expectedResults = generateEventData()
        var actualResult: [EventPlainObject]?

        operationFactoryMock.stubbedUpcomingEvents = expectedResults

        // when
        service.obtainUpcomingEvents(event: event, paginationDirection: .next, successBlock: { events in
            actualResult = events
            expectation.fulfill()
        }, errorBlock: { _ in

        })

        // then
        waitForExpectations(timeout: 0.3) { _ in
            let resultIsEqual = actualResult?.elementsEqual(expectedResults, by: { $0 === $1 })
            expect(resultIsEqual).to(beTrue())
        }
    }

    func testThatServiceReturnsSuccessBlockOnObtainUpcomingEventsForRefresh() {
        // given
        let expectation = self.expectation(description: "Expect that service obtains upcoming events")
        let event = EventPlainObject(id: 0, name: "", location: "", startDate: Date())

        let expectedResults = generateEventData()
        var actualResult: [EventPlainObject]?

        operationFactoryMock.stubbedUpcomingEvents = expectedResults

        // when
        service.obtainUpcomingEvents(event: event, paginationDirection: .previous, successBlock: { events in
            actualResult = events
            expectation.fulfill()
        }, errorBlock: { _ in

        })

        // then
        waitForExpectations(timeout: 0.3) { _ in
            let resultIsEqual = actualResult?.elementsEqual(expectedResults, by: { $0 === $1 })
            expect(resultIsEqual).to(beTrue())
        }
    }

    func testThatServiceReturnsSuccessBlockOnObtainUpcomingEventsEmptyList() {
        // given
        let expectation = self.expectation(description: "Expect that service obtains upcoming events")

        let expectedResults = generateEventData()
        var actualResult: [EventPlainObject]?

        operationFactoryMock.stubbedUpcomingEvents = expectedResults

        // when
        service.obtainUpcomingEvents(event: nil, paginationDirection: .previous, successBlock: { events in
            actualResult = events
            expectation.fulfill()
        }, errorBlock: { _ in

        })

        // then
        waitForExpectations(timeout: 0.3) { _ in
            let resultIsEqual = actualResult?.elementsEqual(expectedResults, by: { $0 === $1 })
            expect(resultIsEqual).to(beTrue())
        }
    }

    func testThatServiceReturnsErrorBlockOnObtainUpcomingEventsForPagination() {
        // given
        let expectation = self.expectation(description: "Expect that service fails to obtain upcoming events")
        let event = EventPlainObject(id: 0, name: "", location: "", startDate: Date())

        let expectedError = NSError(domain: "com.testdomain", code: 0, userInfo: nil)
        var actualError: NSError?

        operationFactoryMock.stubbedError = expectedError

        // when
        service.obtainUpcomingEvents(event: event, paginationDirection: .next, successBlock: { _ in

        }, errorBlock: { error in
            actualError = error as NSError
            expectation.fulfill()
        })

        // then
        waitForExpectations(timeout: 0.3) { _ in
            expect(expectedError).to(be(actualError))
        }
    }

    func testThatServiceReturnsSuccessBlockOnObtainPastEventsEmptyList() {
        // given
        let expectation = self.expectation(description: "Expect that service obtains upcoming events")

        let expectedResults = generateEventData()
        var actualResult: [EventPlainObject]?

        operationFactoryMock.stubbedPastEvents = expectedResults

        // when
        service.obtainPastEvents(event: nil, paginationDirection: .previous, successBlock: { events in
            actualResult = events
            expectation.fulfill()
        }, errorBlock: { _ in

        })

        // then
        waitForExpectations(timeout: 0.3) { _ in
            let resultIsEqual = actualResult?.elementsEqual(expectedResults, by: { $0 === $1 })
            expect(resultIsEqual).to(beTrue())
        }
    }

    func testThatServiceReturnsErrorBlockOnObtainUpcomingEventsForRefresh() {
        // given
        let expectation = self.expectation(description: "Expect that service fails to obtain upcoming events")
        let event = EventPlainObject(id: 0, name: "", location: "", startDate: Date())

        let expectedError = NSError(domain: "com.testdomain", code: 0, userInfo: nil)
        var actualError: NSError?

        operationFactoryMock.stubbedError = expectedError

        // when
        service.obtainUpcomingEvents(event: event, paginationDirection: .previous, successBlock: { _ in

        }, errorBlock: { error in
            actualError = error as NSError
            expectation.fulfill()
        })

        // then
        waitForExpectations(timeout: 0.3) { _ in
            expect(expectedError).to(be(actualError))
        }
    }

    func testThatServiceReturnsSuccessBlockOnObtainPastEventsForPagination() {
        // given
        let expectation = self.expectation(description: "Expect that service obtains past events")
        let event = EventPlainObject(id: 0, name: "", location: "", startDate: Date())

        let expectedResults = generateEventData()
        var actualResult: [EventPlainObject]?

        operationFactoryMock.stubbedPastEvents = expectedResults

        // when
        service.obtainPastEvents(event: event, paginationDirection: .next, successBlock: { events in
            actualResult = events
            expectation.fulfill()
        }, errorBlock: { _ in

        })

        // then
        waitForExpectations(timeout: 0.3) { _ in
            let resultIsEqual = actualResult?.elementsEqual(expectedResults, by: { $0 === $1 })
            expect(resultIsEqual).to(beTrue())
        }
    }

    func testThatServiceReturnsErrorBlockOnObtainPastEventsForPagination() {
        // given
        let expectation = self.expectation(description: "Expect that service fails to obtain past events")
        let event = EventPlainObject(id: 0, name: "", location: "", startDate: Date())

        let expectedError = NSError(domain: "com.testdomain", code: 0, userInfo: nil)
        var actualError: NSError?

        operationFactoryMock.stubbedError = expectedError

        // when
        service.obtainPastEvents(event: event, paginationDirection: .next, successBlock: { _ in

        }, errorBlock: { error in
            actualError = error as NSError
            expectation.fulfill()
        })

        // then
        waitForExpectations(timeout: 0.3) { _ in
            expect(expectedError).to(be(actualError))
        }
    }

    private func generateQuery() -> EventsListPaginationQuery {
        return EventsListPaginationQuery(paginateTo: .next, paginateThrough: Date(), lastID: 0)
    }

    private func generateEventData() -> [EventPlainObject] {
        return [
            EventPlainObject(id: 0, name: "", location: "", startDate: Date()),
            EventPlainObject(id: 0, name: "", location: "", startDate: Date())
        ]
    }
}
