//
//  EventsListOperationFactoryMock.swift
//  picmeTests
//
//  Created by Kirill Makarenko on 02.11.17.
//  Copyright © 2017 S Media Link. All rights reserved.
//

import COOperation
@testable import picme

class EventOperationFactoryMock: EventOperationFactory {
    var invokedEventOperation = false
    var invokedEventOperationCount = 0
    var invokedEventOperationParameters: (resultBlock: CompoundOperationResultBlock?, Void)?
    var invokedEventOperationParametersList = [(resultBlock: CompoundOperationResultBlock?, Void)]()
    var stubbedEventOperationResult: CompoundOperation!
    func eventOperation(with resultBlock: CompoundOperationResultBlock?) -> CompoundOperation {
        invokedEventOperation = true
        invokedEventOperationCount += 1
        invokedEventOperationParameters = (resultBlock, ())
        invokedEventOperationParametersList.append((resultBlock, ()))
        return stubbedEventOperationResult
    }
    var invokedEventSearchOperation = false
    var invokedEventSearchOperationCount = 0
    var invokedEventSearchOperationParameters: (query: EventListQuery, resultBlock: CompoundOperationResultBlock?)?
    var invokedEventSearchOperationParametersList = [(query: EventListQuery, resultBlock: CompoundOperationResultBlock?)]()
    var stubbedEventSearchOperationResult: CompoundOperation!
    func eventSearchOperation(with query: EventListQuery,
                              resultBlock: CompoundOperationResultBlock?) -> CompoundOperation {
        invokedEventSearchOperation = true
        invokedEventSearchOperationCount += 1
        invokedEventSearchOperationParameters = (query, resultBlock)
        invokedEventSearchOperationParametersList.append((query, resultBlock))
        return stubbedEventSearchOperationResult
    }
    var stubbedError: NSError?

    var invokedGetUpcomingEventsOperation = false
    var invokedGetUpcomingEventsOperationCount = 0
    var invokedGetUpcomingEventsOperationParameters: (query: EventsListPaginationQuery?, resultBlock: CompoundOperationResultBlock?)?
    var invokedGetUpcomingEventsOperationParametersList = [(query: EventsListPaginationQuery?, resultBlock: CompoundOperationResultBlock?)]()
    var stubbedGetUpcomingEventsOperationResult: CompoundOperation!

    var stubbedUpcomingEvents: [EventPlainObject] = []

    func getUpcomingEventsOperation(with query: EventsListPaginationQuery?,
                                    and resultBlock: CompoundOperationResultBlock?) -> CompoundOperation {
        invokedGetUpcomingEventsOperation = true
        invokedGetUpcomingEventsOperationCount += 1
        invokedGetUpcomingEventsOperationParameters = (query, resultBlock)
        invokedGetUpcomingEventsOperationParametersList.append((query, resultBlock))

        if let block = resultBlock {
            block(stubbedUpcomingEvents, stubbedError)
        }

        return stubbedGetUpcomingEventsOperationResult ?? CompoundOperation.default()
    }
    var invokedGetPastEventsOperation = false
    var invokedGetPastEventsOperationCount = 0
    var invokedGetPastEventsOperationParameters: (query: EventsListPaginationQuery?, resultBlock: CompoundOperationResultBlock?)?
    var invokedGetPastEventsOperationParametersList = [(query: EventsListPaginationQuery?, resultBlock: CompoundOperationResultBlock?)]()
    var stubbedGetPastEventsOperationResult: CompoundOperation!

    var stubbedPastEvents: [EventPlainObject] = []

    func getPastEventsOperation(with query: EventsListPaginationQuery?,
                                and resultBlock: CompoundOperationResultBlock?) -> CompoundOperation {
        invokedGetPastEventsOperation = true
        invokedGetPastEventsOperationCount += 1
        invokedGetPastEventsOperationParameters = (query, resultBlock)
        invokedGetPastEventsOperationParametersList.append((query, resultBlock))

        if let block = resultBlock {
            block(stubbedPastEvents, stubbedError)
        }

        return stubbedGetPastEventsOperationResult ?? CompoundOperation.default()
    }
}
