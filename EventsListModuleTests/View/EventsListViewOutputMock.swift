@testable import "PROJECT_NAME"

class EventsListViewOutputMock: EventsListViewOutput {
    var invokedDidTriggerViewReadyEvent = false
    var invokedDidTriggerViewReadyEventCount = 0
    func didTriggerViewReadyEvent() {
        invokedDidTriggerViewReadyEvent = true
        invokedDidTriggerViewReadyEventCount += 1
    }
    var invokedDidTriggerLoadAdditionalContent = false
    var invokedDidTriggerLoadAdditionalContentCount = 0
    func didTriggerLoadAdditionalContent() {
        invokedDidTriggerLoadAdditionalContent = true
        invokedDidTriggerLoadAdditionalContentCount += 1
    }
    var invokedDidTriggerRefreshContent = false
    var invokedDidTriggerRefreshContentCount = 0
    func didTriggerRefreshContent() {
        invokedDidTriggerRefreshContent = true
        invokedDidTriggerRefreshContentCount += 1
    }
    var invokedDidTriggerSelectCell = false
    var invokedDidTriggerSelectCellCount = 0
    var invokedDidTriggerSelectCellParameters: (event: EventPlainObject, Void)?
    var invokedDidTriggerSelectCellParametersList = [(event: EventPlainObject, Void)]()
    func didTriggerSelectCell(with event: EventPlainObject) {
        invokedDidTriggerSelectCell = true
        invokedDidTriggerSelectCellCount += 1
        invokedDidTriggerSelectCellParameters = (event, ())
        invokedDidTriggerSelectCellParametersList.append((event, ()))
    }
    var invokedDidTiggerRetryObtainEvents = false
    var invokedDidTiggerRetryObtainEventsCount = 0
    func didTiggerRetryObtainEvents() {
        invokedDidTiggerRetryObtainEvents = true
        invokedDidTiggerRetryObtainEventsCount += 1
    }
}
