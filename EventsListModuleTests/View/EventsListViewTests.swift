import XCTest
import Nimble
@testable import "PROJECT_NAME"

class EventsListViewControllerTests: XCTestCase {

    var viewController: EventsListViewController!
    var outputMock: EventsListViewOutputMock!

    override func setUp() {
        super.setUp()

        viewController = EventsListViewController()

        outputMock = EventsListViewOutputMock()
        viewController.output = outputMock
    }

    override func tearDown() {
        viewController = nil

        super.tearDown()
    }

    func testSuccessViewDidLoad() {
        // given

        // when
        viewController.viewDidLoad()

        // then
        expect(self.outputMock.invokedDidTriggerViewReadyEvent).to(beTrue())
    }

    func testThatViewCallsPresenterOnTapEventCell() {
        // given
        let event = EventPlainObject(id: 0, name: "", location: "", startDate: Date())

        // when
        viewController.didTapCell(with: event)

        // then
        expect(self.outputMock.invokedDidTriggerSelectCellCount).to(equal(1))
        expect(self.outputMock.invokedDidTriggerSelectCellParameters?.event === event).to(beTrue())
    }

    func testThatViewCallsPresenterToLoadAdditionalContent() {
        // given

        // when
        viewController.loadAdditionalContent()

        // then
        expect(self.outputMock.invokedDidTriggerLoadAdditionalContentCount).to(equal(1))
    }
}
