import XCTest
import Nimble
import ViperKit
import Dip
import DipUtils
@testable import "PROJECT_NAME"

class EventsListAssemblyTests: XCTestCase {
    var viewController: EventsListViewController?
    var presenter: EventsListPresenter?
    var interactor: EventsListInteractor?
    var router: EventsListRouterInput?

    var container: DependencyContainer!
    var holder: AssembliesHolder!

    override func setUp() {
        let assemblies = AssembliesCollector.collectAssemblies()
        holder = AssembliesHolder(assemblies: assemblies)

        do {
            let pair = try holder.getAssemblingPair(for: EventsListModuleAssembly.self)
            container = pair.1
        } catch {
            fatalError(error.localizedDescription)
        }

        viewController = try? container.resolve()
        presenter = viewController?.output as? EventsListPresenter
        interactor = presenter?.interactor as? EventsListInteractor
        router = presenter?.router

        super.setUp()
    }

    override func tearDown() {
        viewController = nil
        presenter = nil
        interactor = nil
        router = nil

        container = nil
        holder = nil

        super.tearDown()
    }

    func testThatAssemblyCreatesView() {
        // given

        // when

        // then
        expect(self.viewController).to(beAKindOf(TransitionHandler.self),
                description: "EventsListViewController doesn't conform to TransitionProtocol"
        )
        expect(self.viewController?.output).toNot(beNil(),
                description: "EventsListViewController is nil after assembly"
        )
        expect(self.viewController?.output).to(beAKindOf(EventsListPresenter.self),
                description: "output is not EventsListPresenter"
        )
    }

    func testThatAssemblyCreatesPresenter() {
        // given

        // when

        // then
        expect(self.presenter?.view).toNot(beNil(),
                description: "view in EventsListPresenter is nil after assembly"
        )
        expect(self.presenter?.view).to(beAKindOf(EventsListViewController.self),
                description: "view in EventsListPresenter is not EventsListViewController"
        )
        expect(self.presenter?.interactor).to(beAKindOf(EventsListInteractor.self),
                description: "interactor in EventsListPresenter is not EventsListViewController"
        )
        expect(self.presenter?.router).to(beAKindOf(EventsListRouter.self),
                description: "router in EventsListPresenter is not EventsListRouter"
        )
    }

    func testThatAssemblyCreatesInteractor() {
        // given

        // when

        // then
        expect(self.interactor?.output).toNot(beNil(),
                description: "output of EventsListInteractor is nil after assembly"
        )
        expect(self.interactor?.output).to(beAKindOf(EventsListPresenter.self),
                description: "output in EventsListInteractor is not EventsListPresenter"
        )
    }
}
