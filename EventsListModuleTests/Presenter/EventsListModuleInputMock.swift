@testable import "PROJECT_NAME"

class EventsListModuleInputMock: EventsListModuleInput {
    var invokedConfigureForUpcomingEvents = false
    var invokedConfigureForUpcomingEventsCount = 0
    func configureForUpcomingEvents() {
        invokedConfigureForUpcomingEvents = true
        invokedConfigureForUpcomingEventsCount += 1
    }
    var invokedConfigureForPastEvents = false
    var invokedConfigureForPastEventsCount = 0
    func configureForPastEvents() {
        invokedConfigureForPastEvents = true
        invokedConfigureForPastEventsCount += 1
    }
}
