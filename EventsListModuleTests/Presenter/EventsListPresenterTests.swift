import XCTest
import Nimble
@testable import "PROJECT_NAME"

class EventsListPresenterTests: XCTestCase {

    var presenter: EventsListPresenter!
    var viewMock: EventsListViewInputMock!
    var interactorMock: EventsListInteractorMock!
    var routerMock: EventsListRouterInputMock!
    var stateStorage: EventsListPresenterStateStorage!

    override func setUp() {
        super.setUp()

        viewMock = EventsListViewInputMock()
        interactorMock = EventsListInteractorMock()
        routerMock = EventsListRouterInputMock()
        stateStorage = EventsListPresenterStateStorage()

        presenter = EventsListPresenter(view: viewMock, interactor: interactorMock, router: routerMock, stateStorage: stateStorage)
    }

    override func tearDown() {
        viewMock = nil
        interactorMock = nil
        routerMock = nil
        stateStorage = nil

        presenter = nil

        super.tearDown()
    }

    func testThatPresenterSetupsView() {
        // given

        // when
        presenter.didTriggerViewReadyEvent()

        // then
        expect(self.viewMock.invokedSetupInitialState).to(beTrue())
    }

    func testThatPresenterCallsInteractorToObtainUpcomingEvents() {
        // given
        presenter.eventsTypeState = .upcoming

        // when
        presenter.didTriggerViewReadyEvent()

        // then
        expect(self.interactorMock.invokedObtainUpcomingEventsCount).to(equal(1))
    }

    func testThatPresenterCallsInteractorToObtainPastEvents() {
        // given
        presenter.eventsTypeState = .past

        // when
        presenter.didTriggerViewReadyEvent()

        // then
        expect(self.interactorMock.invokedObtainPastEventsCount).to(equal(1))
    }

    func testThatPresenterUpdatesViewStateWithEvents() {
        // given
        let inputData = generateTestData()

        // when
        presenter.didObtain(events: inputData, paginationDirection: nil)

        // then
        expect(self.viewMock.invokedUpdateStateCount).to(equal(1))

        let resultsIsEquals = viewMock.invokedUpdateStateParameters?.eventsList.elementsEqual(inputData, by: {$0 === $1})
        expect(resultsIsEquals).to(beTrue())
    }

    func testThatPresenterConfiguresStateForUpcomingEvents() {
        // given

        // when
        presenter.configureForUpcomingEvents()

        // then
        expect(self.presenter.eventsTypeState == .upcoming).to(beTrue())
    }

    func testThatPresenterConfiguresStateForPastEvents() {
        // given

        // when
        presenter.configureForPastEvents()

        // then
        expect(self.presenter.eventsTypeState == .past).to(beTrue())
    }

    func testThatPresenterCallsProperMethodsOnObtainEmptyEventsList() {
        // given
        let emptyEventsList: [EventPlainObject] = []

        // when
        presenter.didObtain(events: emptyEventsList, paginationDirection: nil)

        // then
        expect(self.viewMock.invokedHideActivityIndicatorCount).to(equal(1))
        expect(self.viewMock.invokedDidObtainEmptyEventsListCount).to(equal(1))
    }

    func testThatPresenterUpdatesFirstAndLastEventsAfterObtainNonEmptyEventsListWithNoPaginationDirection() {
        // given
        let eventList = generateTestData()

        // when
        presenter.didObtain(events: eventList, paginationDirection: nil)

        // then
        expect(self.presenter.stateStorage.firstEvent === eventList.first).to(beTrue())
        expect(self.presenter.stateStorage.lastEvent === eventList.last).to(beTrue())
        expect(self.viewMock.invokedUpdateStateCount).to(equal(1))

        let resultsIsEqual = viewMock.invokedUpdateStateParameters?.eventsList.elementsEqual(eventList, by: { $0 === $1 })
        expect(resultsIsEqual).to(beTrue())
    }

    func testThatPresenterUpdatesFirstAndLastEventsAfterObtainNonEmptyEventsListWithNextPaginationDirection() {
        // given
        let eventList = generateTestData()

        // when
        presenter.didObtain(events: eventList, paginationDirection: .next)

        // then
        expect(self.stateStorage.lastEvent === eventList.last).to(beTrue())
        expect(self.viewMock.invokedUpdateStateCount).to(equal(1))

        let resultsIsEqual = viewMock.invokedUpdateStateParameters?.eventsList.elementsEqual(eventList, by: { $0 === $1 })
        expect(resultsIsEqual).to(beTrue())
    }

    func testThatPresenterUpdatesFirstAndLastEventsAfterObtainNonEmptyEventsListWithPreviousPaginationDirection() {
        // given
        let eventList = generateTestData()

        // when
        presenter.didObtain(events: eventList, paginationDirection: .previous)

        // then
        expect(self.stateStorage.firstEvent === eventList.first).to(beTrue())
        expect(self.viewMock.invokedUpdateStateCount).to(equal(1))

        let resultsIsEqual = viewMock.invokedUpdateStateParameters?.eventsList.elementsEqual(eventList, by: { $0 === $1 })
        expect(resultsIsEqual).to(beTrue())
    }

    func testThatPresenterUpdatesViewWithUpcomingEventsOnPagination() {
        // given
        presenter.stateStorage.lastEvent = EventPlainObject(id: 0, name: "", location: "", startDate: Date())

        // when
        presenter.eventsTypeState = .upcoming
        presenter.didTriggerLoadAdditionalContent()

        // then
        expect(self.interactorMock.invokedObtainUpcomingEventsCount).to(equal(1))
        expect(self.interactorMock.invokedObtainUpcomingEventsParameters?.event === self.presenter.stateStorage.lastEvent).to(beTrue())
        expect(self.interactorMock.invokedObtainUpcomingEventsParameters?.paginationDirection == .next).to(beTrue())
    }

    func testThatPresenterUpdatesViewWithUpcomingEventsOnRefresh() {
        // given
        presenter.stateStorage.firstEvent = EventPlainObject(id: 0, name: "", location: "", startDate: Date())

        // when
        presenter.eventsTypeState = .upcoming
        presenter.didTriggerRefreshContent()

        // then
        expect(self.interactorMock.invokedObtainUpcomingEventsCount).to(equal(1))
        expect(self.interactorMock.invokedObtainUpcomingEventsParameters?.event === self.presenter.stateStorage.firstEvent).to(beTrue())
        expect(self.interactorMock.invokedObtainUpcomingEventsParameters?.paginationDirection == .previous).to(beTrue())
    }

    func testThatPresenterUpdatesViewWithPastEventsOnPagination() {
        // given
        presenter.stateStorage.lastEvent = EventPlainObject(id: 0, name: "", location: "", startDate: Date())

        // when
        presenter.eventsTypeState = .past
        presenter.didTriggerLoadAdditionalContent()

        // then
        expect(self.interactorMock.invokedObtainPastEventsCount).to(equal(1))
        expect(self.interactorMock.invokedObtainPastEventsParameters?.event === self.presenter.stateStorage.lastEvent).to(beTrue())
        expect(self.interactorMock.invokedObtainPastEventsParameters?.paginationDirection == .next).to(beTrue())
    }

    func testThatPresenterUpdatesViewWithPastEventsOnRefresh() {
        // given
        presenter.stateStorage.firstEvent = EventPlainObject(id: 0, name: "", location: "", startDate: Date())

        // when
        presenter.eventsTypeState = .past
        presenter.didTriggerRefreshContent()

        // then
        expect(self.interactorMock.invokedObtainPastEventsCount).to(equal(1))
        expect(self.interactorMock.invokedObtainPastEventsParameters?.event === self.presenter.stateStorage.firstEvent).to(beTrue())
        expect(self.interactorMock.invokedObtainPastEventsParameters?.paginationDirection == .previous).to(beTrue())
    }

    func testThatPresenterCallsRouterOnTapEventCell() {
        // given
        let event = EventPlainObject(id: 0, name: "", location: "", startDate: Date())

        // when
        presenter.didTriggerSelectCell(with: event)

        // then
        expect(self.routerMock.invokedOpenEventModuleCount).to(equal(1))
        expect(self.routerMock.invokedOpenEventModuleParameters?.event === event).to(beTrue())
    }

    func testThatPresenterShowsErrorState() {
        // given

        // when
        presenter.didObtainEventsWithError(paginationDirection: nil)

        // then
        expect(self.viewMock.invokedShowErrorStateCount).to(equal(1))
    }

    func testThatPresenterCallsInteractorToObtainPastEventsOnDidTriggerRetryButton() {
        // given
        presenter.eventsTypeState = .past

        // when
        presenter.didTiggerRetryObtainEvents()

        // then
        expect(self.interactorMock.invokedObtainPastEventsParameters?.event).to(beNil())
        expect(self.interactorMock.invokedObtainPastEventsParameters?.paginationDirection).to(beNil())

        expect(self.interactorMock.invokedObtainPastEventsCount).to(equal(1))

        expect(self.presenter.stateStorage.firstEvent).to(beNil())
        expect(self.presenter.stateStorage.lastEvent).to(beNil())
    }

    func testThatPresenterCallsInteractorToObtainUpcomingEventsOnDidTriggerRetryButton() {
        // given
        presenter.eventsTypeState = .upcoming

        // when
        presenter.didTiggerRetryObtainEvents()

        // then
        expect(self.interactorMock.invokedObtainUpcomingEventsParameters?.event).to(beNil())
        expect(self.interactorMock.invokedObtainUpcomingEventsParameters?.paginationDirection).to(beNil())

        expect(self.interactorMock.invokedObtainUpcomingEventsCount).to(equal(1))

        expect(self.presenter.stateStorage.firstEvent).to(beNil())
        expect(self.presenter.stateStorage.lastEvent).to(beNil())
    }

    private func generateTestData() -> [EventPlainObject] {
        return [
            EventPlainObject(id: 0, name: "", location: "", startDate: Date()),
            EventPlainObject(id: 0, name: "", location: "", startDate: Date())
        ]
    }
}
