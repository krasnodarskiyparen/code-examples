@testable import "PROJECT_NAME"

class EventsListInteractorMock: EventsListInteractorInput {
    var invokedObtainUpcomingEvents = false
    var invokedObtainUpcomingEventsCount = 0
    var invokedObtainUpcomingEventsParameters: (event: EventPlainObject?, paginationDirection: PaginationDirection?)?
    var invokedObtainUpcomingEventsParametersList = [(event: EventPlainObject?, paginationDirection: PaginationDirection?)]()
    func obtainUpcomingEvents(event: EventPlainObject?, paginationDirection: PaginationDirection?) {
        invokedObtainUpcomingEvents = true
        invokedObtainUpcomingEventsCount += 1
        invokedObtainUpcomingEventsParameters = (event, paginationDirection)
        invokedObtainUpcomingEventsParametersList.append((event, paginationDirection))
    }
    var invokedObtainPastEvents = false
    var invokedObtainPastEventsCount = 0
    var invokedObtainPastEventsParameters: (event: EventPlainObject?, paginationDirection: PaginationDirection?)?
    var invokedObtainPastEventsParametersList = [(event: EventPlainObject?, paginationDirection: PaginationDirection?)]()
    func obtainPastEvents(event: EventPlainObject?, paginationDirection: PaginationDirection?) {
        invokedObtainPastEvents = true
        invokedObtainPastEventsCount += 1
        invokedObtainPastEventsParameters = (event, paginationDirection)
        invokedObtainPastEventsParametersList.append((event, paginationDirection))
    }
}
