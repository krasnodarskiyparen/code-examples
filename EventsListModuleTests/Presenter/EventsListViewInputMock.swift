@testable import "PROJECT_NAME"

class EventsListViewInputMock: EventsListViewInput {
    var invokedSetupInitialState = false
    var invokedSetupInitialStateCount = 0
    func setupInitialState() {
        invokedSetupInitialState = true
        invokedSetupInitialStateCount += 1
    }
    var invokedConfigure = false
    var invokedConfigureCount = 0
    var invokedConfigureParameters: (title: String, Void)?
    var invokedConfigureParametersList = [(title: String, Void)]()
    func configure(with title: String) {
        invokedConfigure = true
        invokedConfigureCount += 1
        invokedConfigureParameters = (title, ())
        invokedConfigureParametersList.append((title, ()))
    }
    var invokedUpdateState = false
    var invokedUpdateStateCount = 0
    var invokedUpdateStateParameters: (eventsList: [EventPlainObject], paginationDirection: PaginationDirection?)?
    var invokedUpdateStateParametersList = [(eventsList: [EventPlainObject], paginationDirection: PaginationDirection?)]()
    func updateState(with eventsList: [EventPlainObject], and paginationDirection: PaginationDirection?) {
        invokedUpdateState = true
        invokedUpdateStateCount += 1
        invokedUpdateStateParameters = (eventsList, paginationDirection)
        invokedUpdateStateParametersList.append((eventsList, paginationDirection))
    }
    var invokedHideActivityIndicator = false
    var invokedHideActivityIndicatorCount = 0
    func hideActivityIndicator() {
        invokedHideActivityIndicator = true
        invokedHideActivityIndicatorCount += 1
    }
    var invokedDidObtainEmptyEventsList = false
    var invokedDidObtainEmptyEventsListCount = 0
    func didObtainEmptyEventsList() {
        invokedDidObtainEmptyEventsList = true
        invokedDidObtainEmptyEventsListCount += 1
    }
    var invokedShowDefaultState = false
    var invokedShowDefaultStateCount = 0
    func showDefaultState() {
        invokedShowDefaultState = true
        invokedShowDefaultStateCount += 1
    }
    var invokedShowEmptyState = false
    var invokedShowEmptyStateCount = 0
    func showEmptyState() {
        invokedShowEmptyState = true
        invokedShowEmptyStateCount += 1
    }
    var invokedShowErrorState = false
    var invokedShowErrorStateCount = 0
    func showErrorState() {
        invokedShowErrorState = true
        invokedShowErrorStateCount += 1
    }
    var invokedShowLoadingState = false
    var invokedShowLoadingStateCount = 0
    func showLoadingState() {
        invokedShowLoadingState = true
        invokedShowLoadingStateCount += 1
    }
}
