@testable import "PROJECT_NAME"

class EventsListRouterInputMock: EventsListRouterInput {
    var invokedOpenEventModule = false
    var invokedOpenEventModuleCount = 0
    var invokedOpenEventModuleParameters: (event: EventPlainObject, Void)?
    var invokedOpenEventModuleParametersList = [(event: EventPlainObject, Void)]()
    func openEventModule(with event: EventPlainObject) {
        invokedOpenEventModule = true
        invokedOpenEventModuleCount += 1
        invokedOpenEventModuleParameters = (event, ())
        invokedOpenEventModuleParametersList.append((event, ()))
    }
}
