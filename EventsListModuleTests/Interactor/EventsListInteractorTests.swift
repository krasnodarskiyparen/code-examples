import XCTest
import Nimble
@testable import "PROJECT_NAME"

class EventsListInteractorTests: XCTestCase {

    var interactor: EventsListInteractor!
    var outputMock: EventsListInteractorOutputMock!
    var eventServiceMock: EventServiceMock!

    override func setUp() {
        super.setUp()

        outputMock = EventsListInteractorOutputMock()
        eventServiceMock = EventServiceMock()

        interactor = EventsListInteractor(eventService: eventServiceMock)
        interactor.output = outputMock
    }

    override func tearDown() {
        interactor = nil
        outputMock = nil

        super.tearDown()
    }

    func testSuccessObtainUpcomingEventsForPaginationWithLastEvent() {
        // given
        let event = EventPlainObject(id: 0, name: "", location: "", startDate: Date())
        let expectedResult = [event, event]

        // when
        eventServiceMock.stubbedUpcomingEvents = expectedResult
        interactor.obtainUpcomingEvents(event: event, paginationDirection: .next)

        // then
        expect(self.outputMock.invokedDidObtainCount).to(equal(1))

        let resultIsEqual = outputMock.invokedDidObtainParameters?.events.elementsEqual(expectedResult, by: { $0 === $1 })
        expect(resultIsEqual).to(beTrue())
    }

    func testSuccessObtainUpcomingEventsForRefreshWithLastEvent() {
        // given
        let event = EventPlainObject(id: 0, name: "", location: "", startDate: Date())
        let expectedResult = [event, event]

        // when
        eventServiceMock.stubbedUpcomingEvents = expectedResult
        interactor.obtainUpcomingEvents(event: event, paginationDirection: .previous)

        // then
        expect(self.outputMock.invokedDidObtainCount).to(equal(1))

        let resultIsEqual = outputMock.invokedDidObtainParameters?.events.elementsEqual(expectedResult, by: { $0 === $1 })
        expect(resultIsEqual).to(beTrue())
    }

    func testSuccessObtainUpcomingEvents() {
        // given
        let event = EventPlainObject(id: 0, name: "", location: "", startDate: Date())
        let expectedResult = [event, event]

        // when
        eventServiceMock.stubbedUpcomingEvents = expectedResult
        interactor.obtainUpcomingEvents(event: nil, paginationDirection: nil)

        // then
        expect(self.outputMock.invokedDidObtainCount).to(equal(1))

        let resultIsEqual = outputMock.invokedDidObtainParameters?.events.elementsEqual(expectedResult, by: { $0 === $1 })
        expect(resultIsEqual).to(beTrue())
    }

    func testErrorObtainUpcomingEvents() {
        // given
        let error = NSError()

        // when
        eventServiceMock.stubbedError = error
        interactor.obtainUpcomingEvents(event: nil, paginationDirection: nil)

        // then
        expect(self.outputMock.invokedDidObtainEventsWithErrorCount).to(equal(1))
    }

    func testErrorObtainUpcomingEventsForPaginationOrRefresh() {
        // given
        let error = NSError()
        let lastEvent = EventPlainObject(id: 0, name: "", location: "", startDate: Date())

        // when
        eventServiceMock.stubbedError = error
        interactor.obtainUpcomingEvents(event: lastEvent, paginationDirection: .next)

        // then
        expect(self.outputMock.invokedDidObtainEventsWithErrorCount).to(equal(1))
    }

    func testSuccessObtainPastEventsForPaginationWithLastEvent() {
        // given
        let event = EventPlainObject(id: 0, name: "", location: "", startDate: Date())
        let expectedResult = [event, event]

        // when
        eventServiceMock.stubbedPastEvents = expectedResult
        interactor.obtainPastEvents(event: event, paginationDirection: .next)

        // then
        expect(self.outputMock.invokedDidObtainCount).to(equal(1))

        let resultIsEqual = outputMock.invokedDidObtainParameters?.events.elementsEqual(expectedResult, by: { $0 === $1 })
        expect(resultIsEqual).to(beTrue())
    }

    func testSuccessObtainPastEventsForRefreshWithLastEvent() {
        // given
        let event = EventPlainObject(id: 0, name: "", location: "", startDate: Date())
        let expectedResult = [event, event]

        // when
        eventServiceMock.stubbedPastEvents = expectedResult
        interactor.obtainPastEvents(event: event, paginationDirection: .previous)

        // then
        expect(self.outputMock.invokedDidObtainCount).to(equal(1))

        let resultIsEqual = outputMock.invokedDidObtainParameters?.events.elementsEqual(expectedResult, by: { $0 === $1 })
        expect(resultIsEqual).to(beTrue())
    }

    func testSuccessObtainPastEvents() {
        // given
        let event = EventPlainObject(id: 0, name: "", location: "", startDate: Date())
        let expectedResult = [event, event]

        // when
        eventServiceMock.stubbedPastEvents = expectedResult
        interactor.obtainPastEvents(event: nil, paginationDirection: nil)

        // then
        expect(self.outputMock.invokedDidObtainCount).to(equal(1))

        let resultIsEqual = outputMock.invokedDidObtainParameters?.events.elementsEqual(expectedResult, by: { $0 === $1 })
        expect(resultIsEqual).to(beTrue())
    }

    func testErrorObtainPastEventsForPaginationForPagination() {
        // given
        let error = NSError()
        let event = EventPlainObject(id: 0, name: "", location: "", startDate: Date())

        // when
        eventServiceMock.stubbedError = error
        interactor.obtainPastEvents(event: event, paginationDirection: .next)

        // then
        expect(self.outputMock.invokedDidObtainEventsWithErrorCount).to(equal(1))
    }

    func testErrorObtainPastEventsForPaginationOrRefresh() {
        // given
        let error = NSError()
        let event = EventPlainObject(id: 0, name: "", location: "", startDate: Date())

        // when
        eventServiceMock.stubbedError = error
        interactor.obtainPastEvents(event: event, paginationDirection: .previous)

        // then
        expect(self.outputMock.invokedDidObtainEventsWithErrorCount).to(equal(1))
    }

    func testErrorObtainPastEvents() {
        // given
        let error = NSError()

        // when
        eventServiceMock.stubbedError = error
        interactor.obtainPastEvents(event: nil, paginationDirection: nil)

        // then
        expect(self.outputMock.invokedDidObtainEventsWithErrorCount).to(equal(1))
    }
}
