@testable import "PROJECT_NAME"

class EventsListInteractorOutputMock: EventsListInteractorOutput {
    var invokedDidObtain = false
    var invokedDidObtainCount = 0
    var invokedDidObtainParameters: (events: [EventPlainObject], paginationDirection: PaginationDirection?)?
    var invokedDidObtainParametersList = [(events: [EventPlainObject], paginationDirection: PaginationDirection?)]()
    func didObtain(events: [EventPlainObject], paginationDirection: PaginationDirection?) {
        invokedDidObtain = true
        invokedDidObtainCount += 1
        invokedDidObtainParameters = (events, paginationDirection)
        invokedDidObtainParametersList.append((events, paginationDirection))
    }
    var invokedDidObtainEventsWithError = false
    var invokedDidObtainEventsWithErrorCount = 0
    var invokedDidObtainEventsWithErrorParameters: (paginationDirection: PaginationDirection?, Void)?
    var invokedDidObtainEventsWithErrorParametersList = [(paginationDirection: PaginationDirection?, Void)]()
    func didObtainEventsWithError(paginationDirection: PaginationDirection?) {
        invokedDidObtainEventsWithError = true
        invokedDidObtainEventsWithErrorCount += 1
        invokedDidObtainEventsWithErrorParameters = (paginationDirection, ())
        invokedDidObtainEventsWithErrorParametersList.append((paginationDirection, ()))
    }
}
