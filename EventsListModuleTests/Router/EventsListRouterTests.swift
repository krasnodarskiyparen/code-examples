import XCTest
import Nimble
@testable import ViperKit
@testable import "PROJECT_NAME"

class EventsListRouterTests: XCTestCase {

    var router: EventsListRouter!
    var transitionHandlerMock: TransitionHandlerMock!
    var eventModuleInputMock: EventModuleInputMock!

    override func setUp() {
        super.setUp()
        router = EventsListRouter()
        transitionHandlerMock = TransitionHandlerMock()
        router.transitionHandler = transitionHandlerMock
        eventModuleInputMock = EventModuleInputMock()
    }

    override func tearDown() {
        router = nil
        transitionHandlerMock = nil

        eventModuleInputMock = nil

        super.tearDown()
    }

    func testThatRouterOpenEventModule() {
        // given
        let event = EventPlainObject(id: 0, name: "", location: "", startDate: Date())

        transitionHandlerMock.stubbedModuleInput = eventModuleInputMock

        // when
        router.openEventModule(with: event)

        // then
        expect(self.eventModuleInputMock.invokedConfigureParameters?.event === event).to(beTrue())
    }
}
