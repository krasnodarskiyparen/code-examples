typealias EventServiceCompletionBlock = (_ events: EventsPreview?, _ error: Error?) -> Void
typealias EventServiceErrorBlock = (_ error: Error) -> Void
typealias EventServiceSuccessBlock = (_ events: [EventPlainObject]) -> Void

/// Describes an object responsible for obtain / update event objects
protocol EventService {

    func obtainEvents(with completionBlock: @escaping EventServiceCompletionBlock)

    func obtainEventsList(with searchText: String,
                          completionHandler: @escaping ([EventPlainObject]) -> Void,
                          errorHandler: @escaping EventServiceErrorBlock)

    /// Method is used to obtain upcoming events
    ///
    /// - parameter event: Last or first event in list depending on pagination or refresh(optional)
    /// - parameter paginationDirection: direction of pagination
    /// - parameter successBlock: Returns array of EventPlainObject
    /// - parameter errorBlock: Returns error
    func obtainUpcomingEvents(event: EventPlainObject?,
                              paginationDirection: PaginationDirection?,
                              successBlock: @escaping EventServiceSuccessBlock,
                              errorBlock: @escaping EventServiceErrorBlock)

    /// Method is used to obtain past events
    ///
    /// - parameter event: Last or first event in list depending on pagination or refresh(optional)
    /// - parameter paginationDirection: direction of pagination
    /// - parameter successBlock: Returns array of EventPlainObject
    /// - parameter errorBlock: Returns error
    func obtainPastEvents(event: EventPlainObject?,
                          paginationDirection: PaginationDirection?,
                          successBlock: @escaping EventServiceSuccessBlock,
                          errorBlock: @escaping EventServiceErrorBlock)
}
