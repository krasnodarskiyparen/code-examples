import Foundation

class EventServiceImp: EventService {

    private let operationFactory: EventOperationFactory
    private let operationScheduler: OperationScheduler

    init(eventOperationFactory: EventOperationFactory, operationScheduler: OperationScheduler) {
        self.operationFactory = eventOperationFactory
        self.operationScheduler = operationScheduler
    }

    func obtainEvents(with completionBlock: @escaping EventServiceCompletionBlock) {
        let operation = operationFactory.eventOperation { response, error in

            let eventsPreview = response as? EventsPreview
            DispatchQueue.main.async {
                completionBlock(eventsPreview, error)
            }
        }

        operationScheduler.add(operation)
    }

    func obtainEventsList(with searchText: String,
                          completionHandler: @escaping ([EventPlainObject]) -> Void,
                          errorHandler: @escaping EventServiceErrorBlock) {
        let query = EventListQuery(search: searchText)

        let operation = operationFactory.eventSearchOperation(with: query) { response, error in
            if let unwrappedError = error {
                DispatchQueue.main.async {
                    errorHandler(unwrappedError)
                }
            }

            if let eventsList = response as? [EventPlainObject] {
                DispatchQueue.main.async {
                    completionHandler(eventsList)
                }
            }
        }

        operationScheduler.add(operation)
    }

    func obtainUpcomingEvents(event: EventPlainObject?,
                              paginationDirection: PaginationDirection?,
                              successBlock: @escaping EventServiceSuccessBlock,
                              errorBlock: @escaping EventServiceErrorBlock) {

        let query = generateQuery(from: event, and: paginationDirection)

        let operation = operationFactory.getUpcomingEventsOperation(with: query) { response, error in

            self.errorBlockHandler(errorBlock: errorBlock, error: error)

            self.successBlockHandler(successBlock: successBlock, response: response)
        }

        operationScheduler.add(operation)
    }

    func obtainPastEvents(event: EventPlainObject?,
                          paginationDirection: PaginationDirection?,
                          successBlock: @escaping EventServiceSuccessBlock,
                          errorBlock: @escaping EventServiceErrorBlock) {

        let query = generateQuery(from: event, and: paginationDirection)

        let operation = operationFactory.getPastEventsOperation(with: query) { response, error in

            self.errorBlockHandler(errorBlock: errorBlock, error: error)

            self.successBlockHandler(successBlock: successBlock, response: response)
        }

        operationScheduler.add(operation)
    }

    private func generateQuery(from event: EventPlainObject?, and paginationDirection: PaginationDirection?) -> EventsListPaginationQuery? {

        if let event = event, let paginationDirection = paginationDirection {
            return EventsListPaginationQuery(paginateTo: paginationDirection, paginateThrough: event.startDate, lastID: event.id)
        }

        return nil
    }

    private func successBlockHandler(successBlock: @escaping EventServiceSuccessBlock, response: Any?) {
        if let response = response as? [EventPlainObject] {
            DispatchQueue.main.async {
                successBlock(response)
            }
        }
    }

    private func errorBlockHandler(errorBlock: @escaping EventServiceErrorBlock, error: Error?) {
        if let unwrappedError = error {
            DispatchQueue.main.async {
                errorBlock(unwrappedError)
            }
        }
    }
}
