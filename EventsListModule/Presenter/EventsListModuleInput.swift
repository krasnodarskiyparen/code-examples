import ViperKit

protocol EventsListModuleInput: class, ModuleInput {

    // configure module for upcoming events
    func configureForUpcomingEvents()

    /// configure module for past events
    func configureForPastEvents()
}
