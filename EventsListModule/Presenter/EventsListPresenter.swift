class EventsListPresenter {

    weak var view: EventsListViewInput?
    private let interactor: EventsListInteractorInput
    private let router: EventsListRouterInput
    private let stateStorage: EventsListPresenterStateStorage

    var eventsTypeState: EventsType?

    init(view: EventsListViewInput, interactor: EventsListInteractorInput, router: EventsListRouterInput, stateStorage: EventsListPresenterStateStorage) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.stateStorage = stateStorage
    }

    fileprivate func fillStateStorage(events: [EventPlainObject], paginationDirection: PaginationDirection?) {
        if let paginationDirection = paginationDirection {
            switch paginationDirection {
            case .next:
                stateStorage.lastEvent = events.last
            case .previous:
                stateStorage.firstEvent = events.first
            }
        } else {
            stateStorage.firstEvent = events.first
            stateStorage.lastEvent = events.last
        }
    }
}

// MARK: - EventsListModuleInput

extension EventsListPresenter: EventsListModuleInput {
    func configureForUpcomingEvents() {
        eventsTypeState = .upcoming
        view?.configure(with: "Upcoming")
    }

    func configureForPastEvents() {
        eventsTypeState = .past
        view?.configure(with: "Past")
    }
}

// MARK: - EventsListViewOutput

extension EventsListPresenter: EventsListViewOutput {

    func didTriggerSelectCell(with event: EventPlainObject) {
        router.openEventModule(with: event)
    }

    func didTriggerViewReadyEvent() {
        view?.setupInitialState()

        view?.showLoadingState()
        handleObtainingEvents(paginationDirection: nil, boundaryEvent: nil)
    }

    func didTriggerLoadAdditionalContent() {
        if let boundaryEvent = stateStorage.lastEvent {
            handleObtainingEvents(paginationDirection: .next, boundaryEvent: boundaryEvent)
        }
    }

    func didTriggerRefreshContent() {
        if let boundaryEvent = stateStorage.firstEvent {
            handleObtainingEvents(paginationDirection: .previous, boundaryEvent: boundaryEvent)
        }
    }

    func didTiggerRetryObtainEvents() {
        view?.showLoadingState()
        handleObtainingEvents(paginationDirection: nil, boundaryEvent: nil)
    }

    func handleObtainingEvents(paginationDirection: PaginationDirection?, boundaryEvent: EventPlainObject?) {
        if let eventsTypeState = eventsTypeState {

            switch eventsTypeState {
            case .upcoming:
                interactor.obtainUpcomingEvents(event: boundaryEvent, paginationDirection: paginationDirection)
            case .past:
                interactor.obtainPastEvents(event: boundaryEvent, paginationDirection: paginationDirection)
            }
        }
    }
}

// MARK: - EventsListInteractorOutput

extension EventsListPresenter: EventsListInteractorOutput {

    func didObtain(events: [EventPlainObject], paginationDirection: PaginationDirection?) {

        if events.isEmpty {
            view?.hideActivityIndicator()
            view?.didObtainEmptyEventsList()
            return
        }
        fillStateStorage(events: events, paginationDirection: paginationDirection)

        view?.showDefaultState()
        view?.updateState(with: events, and: paginationDirection)
    }

    func didObtainEventsWithError(paginationDirection: PaginationDirection?) {
        view?.hideActivityIndicator()

        if paginationDirection == nil {
            view?.showErrorState()
        }
    }
}
