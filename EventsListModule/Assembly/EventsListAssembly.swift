import DipUtils
import ViperKit

final class EventsListModuleAssembly: UIAssembly {
    let helpersAssembly: ContainerProvider<PresentationLayerHelpersAssembly> = .init()
    let servicesAssembly: ContainerProvider<ServiceLayerAssembly> = .init()

    func register(into container: DependencyContainer) {
        container.register(tag: nil) { EventsListViewController() }
            .implements(EventsListViewInput.self, TransitionHandler.self)
            .resolvingProperties { container, viewController in
                viewController.output = try container.resolve()
                viewController.displayManager = try container.resolve()
            }

        container.register { EventsListPresenter(view: $0, interactor: $1, router: $2, stateStorage: $3) }
            .implements(EventsListModuleInput.self, EventsListViewOutput.self, EventsListInteractorOutput.self)

        container.register { [unowned self] () -> EventsListInteractor in
            let service: EventService = try self.servicesAssembly.container.resolve()
            return EventsListInteractor(eventService: service)
        }
            .implements(EventsListInteractorInput.self)
            .resolvingProperties { container, interactor in
                interactor.output = try container.resolve()
            }

        container.register { EventsListRouter() }
            .implements(EventsListRouterInput.self)
            .resolvingProperties { container, router in
                router.transitionHandler = try container.resolve(tag: nil) as EventsListViewController
            }

        container.register { [unowned self] () -> EventsListDisplayManager in
            let factory: EventTableViewCellObjectFactory = try self.helpersAssembly.container.resolve()
            let calculator: EventCellSizeCalculator = try self.helpersAssembly.container.resolve()

            return EventsListDisplayManager(cellObjectsFactory: factory, cellSizeCalculator: calculator)
        }

        container.register { EventsListPresenterStateStorage() }
    }
}

extension EventsListViewController: StoryboardInstantiatable {}
