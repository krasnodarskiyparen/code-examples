import UIKit
import DTTableViewManager
import DTModelStorage

final class EventsListDisplayManager: NSObject, DTTableViewManageable {

    weak var delegate: EventsListDisplayManagerDelegate?
    private var contentIsUpdating = true
    
    private var lastContentSize = CGFloat()
    private let cellObjectsFactory: EventTableViewCellObjectFactory!
    private let cellSizeCalculator: EventCellSizeCalculator!

    init(cellObjectsFactory: EventTableViewCellObjectFactory, cellSizeCalculator: EventCellSizeCalculator) {
        super.init()
        
        self.cellObjectsFactory = cellObjectsFactory
        self.cellSizeCalculator = cellSizeCalculator
    }
    
    var tableView: UITableView! {
        didSet {
            manager.startManaging(withDelegate: self)

            manager.register(EventTableViewCell.self)
            manager.heightForCell(withItem: EventTableViewCellObject.self) { _, _ -> CGFloat in
                self.cellSizeCalculator.calculateCellHeight(with: self.tableView.frame.width)
            }

            manager.willDisplay(EventTableViewCell.self) { cell, _, _ in
                cell.selectionStyle = .none
            }
            manager.estimatedHeightForCell(withItem: EventTableViewCellObject.self) { _, _ -> CGFloat in
                self.cellSizeCalculator.calculateCellHeight(with: self.tableView.frame.width)
            }
            manager.didSelect(EventTableViewCell.self) { _, model, _ in
                self.delegate?.didTapCell(with: model.event)
            }
        }
    }

    private let cellObjectsFactory: EventTableViewCellObjectFactory!
    private let cellSizeCalculator: EventCellSizeCalculator!


    func update(with eventsList: [EventPlainObject], and paginationDirection: PaginationDirection?) {

        calculateLastEventsContentSize(eventsCount: eventsList.count)

        if let paginationDirection = paginationDirection, let eventCellObjects = cellObjectsFactory.convertToCellObjects(with: eventsList) {
            switch paginationDirection {
            case .next:
                manager.memoryStorage.addItems(eventCellObjects)
            case .previous:
                for ind in 0...eventCellObjects.count - 1 {
                    try? manager.memoryStorage.insertItem(eventCellObjects[ind], to: IndexPath(row: ind, section: 0))
                }
            }
        } else {
            if let eventCellObjects = cellObjectsFactory.convertToCellObjects(with: eventsList) {
                manager.memoryStorage.addItems(eventCellObjects)
            }
        }
        contentIsUpdating = false
    }

    func endContentUpdate() {
        contentIsUpdating = false
    }

    private func calculateLastEventsContentSize(eventsCount: Int) {
        lastContentSize = CGFloat(eventsCount) * cellSizeCalculator.calculateCellHeight(with: self.tableView.frame.width)

        if lastContentSize < tableView.frame.size.height {
            lastContentSize = tableView.frame.size.height * 1.5
        }
    }
}

extension EventsListDisplayManager: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - lastContentSize / 2

        let deltaOffset = maximumOffset - currentOffset

        if deltaOffset <= 0 && !contentIsUpdating && currentOffset > 0 {
            delegate?.loadAdditionalContent()
            contentIsUpdating = true
        }
    }
}
