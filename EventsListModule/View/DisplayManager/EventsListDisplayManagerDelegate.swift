protocol EventsListDisplayManagerDelegate: class {

    /// Notify view that user needs more content to be shown in tableview
    func loadAdditionalContent()

    /// Notify view that cell with given event was selected
    func didTapCell(with event: EventPlainObject)
}
