protocol EventsListViewInput: class {

    /// Setup initial state of the view
    func setupInitialState()

    /// Configure view with title
    func configure(with title: String)

    /// Setups view with events list
    ///
    /// - parameter eventsList: An array which contains events objects
    /// - parameter paginationDirection: direction of pagination. May be nil if obtaining first group of events
    func updateState(with eventsList: [EventPlainObject], and paginationDirection: PaginationDirection?)

    /// Hides activity indicator
    func hideActivityIndicator()

    /// Notify view that array of obtained events is empty
    func didObtainEmptyEventsList()

    /// Triggers view to setup default state
    func showDefaultState()

    /// Triggers view to setup empty state
    func showEmptyState()

    /// Triggers view to setup error state
    func showErrorState()

    /// Triggers view to setup loading state
    func showLoadingState()
}

protocol EventsListViewOutput {

    /// Notify presenter that view is ready
    func didTriggerViewReadyEvent()

    /// Notify presenter that additional events should be loaded
    func didTriggerLoadAdditionalContent()

    /// Notify presenter to refresh events list
    func didTriggerRefreshContent()

    /// Notify presenter that cell was selected with given event
    func didTriggerSelectCell(with event: EventPlainObject)

    /// Notify presenter to obtain events again
    func didTiggerRetryObtainEvents()
}
