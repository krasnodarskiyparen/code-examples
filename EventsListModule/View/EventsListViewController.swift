import UIKit
import ViperKit

final class EventsListViewController: BaseViewController {
    
    private enum Constants {
        static let tableViewTopOffset: Int = 16
    }

    var output: EventsListViewOutput?
    var displayManager: EventsListDisplayManager?
    var refreshControl: UIRefreshControl?

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            displayManager?.tableView = tableView
            tableView.contentInset = UIEdgeInsets(top: Constants.tableViewTopOffset,
                                                  left: 0,
                                                  bottom: 0,
                                                  right: 0)
        }
    }

    lazy var issueView = self.setupIssueView()
    lazy var loaderView = self.setupLoaderView()

    // MARK: Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        displayManager?.delegate = self
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(applicationBecomeActive),
                                               name: NSNotification.Name.UIApplicationDidBecomeActive,
                                               object: nil)

        output?.didTriggerViewReadyEvent()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
        navigationController?.navigationBar.becomeDefault()

        setupTableViewPosition()
        setupRefreshConrol()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        refreshControl?.removeFromSuperview()
    }

    @objc func applicationBecomeActive() {
        setupTableViewPosition()
        setupRefreshConrol()
    }

    @objc func didTriggerRefresh() {
        if let displayManager = displayManager {
            if !displayManager.contentIsUpdating {
                output?.didTriggerRefreshContent()
            }
        }
    }

    func setupRefreshConrol() {
        refreshControl?.removeFromSuperview()
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(didTriggerRefresh), for: .valueChanged)
        tableView.addSubview(refreshControl!)
    }

    private func setupTableViewPosition() {
        if tableView.contentOffset.y < -Constants.tableViewTopOffset {
            tableView.contentOffset.y = -Constants.tableViewTopOffset
        }
        tableView.contentInset.top = Constants.tableViewTopOffset
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        loaderView.center = tableView.center
    }

    private func setupIssueView() -> IssueDescriptionView {
        let view = IssueDescriptionView.viewFromNib()
        view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(view)
        view.buttonTapHandler = { [unowned self] in
            self.output?.didTiggerRetryObtainEvents()
        }

        let constraints = view.centerPositionInSuperview()
        self.view.addConstraints(constraints)

        return view
    }

    private func setupLoaderView() -> UIView {
        let loaderConfigurator = LoaderViewConfigurator(view: tableView, loaderPosition: tableView.center)
        return loaderConfigurator.createLoaderView()
    }

    func showDefaultState() {
        loaderView.isHidden = true
        issueView.isHidden = true
        tableView.isUserInteractionEnabled = true
    }

    func showLoadingState() {
        loaderView.isHidden = false
        issueView.isHidden = true
        tableView.isUserInteractionEnabled = false
    }

    func showErrorState() {
        issueView.display(.eventsError)
        issueView.buttonTapHandler = {
            self.output?.didTiggerRetryObtainEvents()
        }
        issueView.isHidden = false
        loaderView.isHidden = true
        tableView.isUserInteractionEnabled = false
    }

    func showEmptyState() {
        issueView.display(.emptyEvents)
        issueView.isHidden = false
        loaderView.isHidden = true
        tableView.isUserInteractionEnabled = false
    }
}

// MARK: - View Input

extension EventsListViewController: EventsListViewInput {

    func setupInitialState() {

    }

    func configure(with title: String) {
        navigationItem.title = title
    }

    func updateState(with eventsList: [EventPlainObject], and paginationDirection: PaginationDirection?) {
        displayManager?.update(with: eventsList, and: paginationDirection)
        hideActivityIndicator()
    }

    func hideActivityIndicator() {
        guard
            let refresh = refreshControl,
            refresh.isRefreshing
            else {
                return
        }

        refresh.endRefreshing()
    }

    func didObtainEmptyEventsList() {
        displayManager?.endContentUpdate()
    }
}

// MARK: - ModuleInputProvider

extension EventsListViewController: ModuleInputProvider {
    var moduleInput: ModuleInput! {
        // swiftlint:disable force_cast
        return output as! EventsListModuleInput
    }
}

extension EventsListViewController: EventsListDisplayManagerDelegate {

    func didTapCell(with event: EventPlainObject) {
        output?.didTriggerSelectCell(with: event)
    }

    func loadAdditionalContent() {
        output?.didTriggerLoadAdditionalContent()
    }
}
