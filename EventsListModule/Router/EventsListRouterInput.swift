protocol EventsListRouterInput {
    func openEventModule(with event: EventPlainObject)
}
