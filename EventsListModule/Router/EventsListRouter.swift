import ViperKit

private enum EventsListRouterSegue {
    static let toEvent = "EventsListModuleToEventModuleSegue"
}

final class EventsListRouter: EventsListRouterInput {
    weak var transitionHandler: TransitionHandler?

    func openEventModule(with event: EventPlainObject) {
        transitionHandler?.openModule(segueIdentifier: EventsListRouterSegue.toEvent, configurationBlock: { moduleInput in
            guard let eventModule = moduleInput as? EventModuleInput else {
                fatalError("ModuleInput doesn't conforms EventModuleInput")
            }

            eventModule.configure(with: event)
        })
    }
}
