import Foundation

final class EventsListInteractor: EventsListInteractorInput {

    weak var output: EventsListInteractorOutput?

    private let eventService: EventService

    init(eventService: EventService) {
        self.eventService = eventService
    }

    func obtainUpcomingEvents(event: EventPlainObject?, paginationDirection: PaginationDirection?) {
        handleObtainingUpcomingEvents(with: event, and: paginationDirection)
    }

    func obtainPastEvents(event: EventPlainObject?, paginationDirection: PaginationDirection?) {
        handleObtainingPastEvents(with: event, and: paginationDirection)
    }

    private func handleObtainingUpcomingEvents(with event: EventPlainObject?, and paginationDirection: PaginationDirection?) {
        eventService.obtainUpcomingEvents(event: event,
                                          paginationDirection: paginationDirection,
                                          successBlock: { events in
        self.output?.didObtain(events: events, paginationDirection: paginationDirection)
        }, errorBlock: { _  in
            self.output?.didObtainEventsWithError(paginationDirection: paginationDirection)
        })
    }

    private func handleObtainingPastEvents(with event: EventPlainObject?, and paginationDirection: PaginationDirection?) {
        eventService.obtainPastEvents(event: event,
                                      paginationDirection: paginationDirection,
                                      successBlock: { events in
        self.output?.didObtain(events: events, paginationDirection: paginationDirection)
        }, errorBlock: { _  in
            self.output?.didObtainEventsWithError(paginationDirection: paginationDirection)
        })
    }
}
