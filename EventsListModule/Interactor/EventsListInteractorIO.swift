import Foundation

protocol EventsListInteractorInput {

    /// Call interactor to obtain upcoming events
    ///
    /// - parameter event: last obtained event. May be nil if obtaining first group of events
    /// - parameter paginationDirection: direction of pagination. May be nil if obtaining first group of events
    func obtainUpcomingEvents(event: EventPlainObject?, paginationDirection: PaginationDirection?)

    /// Call interactor to obtain past events
    ///
    /// - parameter event: last obtained event. May be nil if obtaining first group of events
    /// - parameter paginationDirection: direction of pagination. May be nil if obtaining first group of events
    func obtainPastEvents(event: EventPlainObject?, paginationDirection: PaginationDirection?)
}

protocol EventsListInteractorOutput: class {

    /// Notify presenter about obtained events
    ///
    /// - parameter events: Obtained events
    /// - parameter paginationDirection: Direction of pagination
    func didObtain(events: [EventPlainObject], paginationDirection: PaginationDirection?)

    /// Notify presenter about error while obtaining events
    ///
    /// - parameter paginationDirection: Direction of pagination
    func didObtainEventsWithError(paginationDirection: PaginationDirection?)
}
