import EasyDi

final class BasicAuthorizationModuleAssembly: Assembly {
    
    private lazy var useCasesAssembly: UseCasesAssembly = self.context.assembly()
    private lazy var presentationLayerHelpersAssembly: PresentationLayerHelpersAssembly = self.context.assembly()
    
    var view: BasicAuthorizationView {
        return definePlaceholder()
    }
    
    var state: BasicAuthorizationPresenterState {
        return define(
            scope: .prototype,
            init: BasicAuthorizationPresenterState()
        )
    }
    
    var presenter: BasicAuthorizationPresenter {
        return define(scope: .prototype,
               init: BasicAuthorizationPresenterImp(
                view: self.view,
                state: self.state,
                basicAuthorizationUseCase: self.useCasesAssembly.basicAuthorizationUseCase,
                requestSMSConfirmationUseCase: self.useCasesAssembly.requestSMSConfirmationUseCase,
                clearSessionDetailsUseCase: self.useCasesAssembly.clearSessionDetailsUseCase,
                obtainPincodeCreationParametersUseCase: self.useCasesAssembly.obtainPincodeCreationParametersUseCase)) {
                    $0.errorHandler = self.basicAuthorizationErrorHandler
                    return $0
        }
    }
    
    var basicAuthorizationErrorHandler: BasicAuthorizationErrorHandler {
        return define(scope: .prototype, init: BasicAuthorizationErrorHandler(
            defaultErrorHandler: self.presentationLayerHelpersAssembly.errorHandler,
            requestSMSConfirmationUseCase: self.useCasesAssembly.requestSMSConfirmationUseCase)
        )
    }
    
    func inject(into basicAuthorizationViewController: BasicAuthorizationViewController) {
        defineInjection(key: "view", into: basicAuthorizationViewController) {
            $0.presenter = self.presenter
            return $0
        }
    }
}
