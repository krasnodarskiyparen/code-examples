import UIKit

final class BasicAuthorizationViewController: UIViewController, BasicAuthorizationView {

    private enum Constants {
        static let activeTextFieldTopOffset: CGFloat = 100.0
        static let scrollAnimationDuration: TimeInterval = 0.3
        
        static let loginButtonActiveColor: UIColor = #colorLiteral(red: 0.06274509804, green: 0.3411764706, blue: 0.6549019608, alpha: 1)
        static let loginButtonDisabledColor: UIColor = #colorLiteral(red: 0.06274509804, green: 0.3411764706, blue: 0.6549019608, alpha: 0.5)
    }
    
    var presenter: BasicAuthorizationPresenter!
    var onObtainPincodeParameters: ((String, PincodeCreationParameters) -> Void)?
    var onConfirmationRequired: (() -> Void)?
    
    @IBOutlet weak var contentViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        BasicAuthorizationModuleAssembly.instance().inject(into: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.didTriggerViewReadyEvent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        presenter.didTriggerViewWillAppearEvent()
    }
    
    @IBAction func didTapOnBackground(_ sender: Any?) {
        view.endEditing(true)
    }
    
    @IBAction func didPressLogIn(_ sender: Any?) {
        view.endEditing(true)
        guard
            let username = usernameTextField.text,
            let password = passwordTextField.text
        else { return }
        
        presenter.didTapLogInButton(with: username, and: password)
    }
    
    private func subscribeToTextfieldsEvents() {
        usernameTextField.delegate = self
        usernameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        passwordTextField.delegate = self
        passwordTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
    }
    
    private func subscribeToKeyboardChanges() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard
            let keyboardSize = notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? CGRect,
            let animationDuration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? Double
        else { return }

        contentViewBottomConstraint.constant = keyboardSize.height
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        guard let animationDuration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval else { return }
        contentViewBottomConstraint.constant = 0
        UIView.animate(withDuration: animationDuration) {
            self.scrollView.contentOffset.y = 0
            self.view.layoutIfNeeded()
        }
    }

    
    func setupInitialState() {
        subscribeToKeyboardChanges()
        subscribeToTextfieldsEvents()
        navigationController?.navigationBar.becomeTransparent()
    }
}

extension BasicAuthorizationViewController: SMSConfirmationModuleOutputProvider {
    
    var SMSConfirmationModuleOutput: SMSConfirmationModuleOutput {
        return presenter as! SMSConfirmationModuleOutput
    }
    
}

extension BasicAuthorizationViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: Constants.scrollAnimationDuration) {
            var scrollViewOffset = self.scrollView.contentOffset
            scrollViewOffset.y = textField.frame.origin.y - Constants.activeTextFieldTopOffset
            self.scrollView.contentOffset = scrollViewOffset
        }
    }
    
    @objc func textFieldDidChange(_ sender: UITextField) {
        guard let text = sender.text else { return }
        
        // If we cleared the text or we entered a first character
        if text.count == 0 {
            updateLoginButtonState()
        } else if text.count == 1 {
            updateLoginButtonState()
        }
    }

}

extension BasicAuthorizationViewController {
    
    private func updateLoginButtonState() {
        guard
            let enteredUsername = usernameTextField.text,
            let enteredPassword = passwordTextField.text
        else {
            loginButton.backgroundColor = Constants.loginButtonDisabledColor
            return
        }
        
        let usernameIsEmpty = enteredUsername.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
        let passwordIsEmpty = enteredPassword.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
        
        switch (usernameIsEmpty, passwordIsEmpty) {
        case (false, false):
            loginButton.isEnabled = true
            loginButton.backgroundColor = Constants.loginButtonActiveColor
        default:
            loginButton.isEnabled = false
            loginButton.backgroundColor = Constants.loginButtonDisabledColor
        }
    }
    
}
