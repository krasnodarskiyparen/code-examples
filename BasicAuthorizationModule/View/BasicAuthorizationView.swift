import Foundation

protocol BasicAuthorizationView: BaseView, CanShowError {
    
    var onConfirmationRequired: (() -> Void)? { get set }
    
    var onObtainPincodeParameters: ((_ username: String,
                                    _ pincodeParameters: PincodeCreationParameters) -> Void)? { get set }
    
    func setupInitialState()
}
