import Foundation

protocol BasicAuthorizationPresenter: AutoMockable {
    
    /// Notify presenter that login button was tapped
    ///
    /// - parameter username: username
    /// - parameter password: password
    func didTapLogInButton(with username: String, and password: String)
    
    /// Notify presenter that view is ready
    func didTriggerViewReadyEvent()
    
    /// Notify presenter that view is appearing
    func didTriggerViewWillAppearEvent()
}
