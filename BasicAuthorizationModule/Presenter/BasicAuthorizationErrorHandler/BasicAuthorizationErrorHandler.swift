import Foundation

class BasicAuthorizationErrorHandler: ErrorHandler {

    weak var view: BasicAuthorizationView?
    
    func configure(with view: CanShowError?) {
        
        guard let basicAuthorizationView = view as? BasicAuthorizationView else { return }
        self.view = basicAuthorizationView
        
        defaultErrorHandler.configure(with: view)
    }
    
    private let defaultErrorHandler: ErrorHandler
    private let requestSMSConfirmationUseCase: RequestSMSConfirmationUseCase
    
    init(defaultErrorHandler: ErrorHandler, requestSMSConfirmationUseCase: RequestSMSConfirmationUseCase) {
        self.defaultErrorHandler = defaultErrorHandler
        self.requestSMSConfirmationUseCase = requestSMSConfirmationUseCase
    }
    
    func proceed(error: Error) {
        switch (error as NSError).code {
        case NetworkError.confirmationRequired.rawValue:
            requestSMSConfirmationUseCase.requestSMSConfirmation()
            .then { _ in
                self.view?.onConfirmationRequired?()
            }.always {
                self.view?.stopIndication()
            }.catch { error in
                self.defaultErrorHandler.proceed(error: error)
            }
        default:
            view?.stopIndication(animated: false)
            defaultErrorHandler.proceed(error: error)
        }
    }
}
