import Promises

final class BasicAuthorizationPresenterImp: ErrorHandlerPresenter {
    
    var errorHandler: ErrorHandler?
    weak var view: BasicAuthorizationView?
    
    var state: BasicAuthorizationPresenterState
    
    private let basicAuthorizationUseCase: BasicAuthorizationUseCase
    private let requestSMSConfirmationUseCase: RequestSMSConfirmationUseCase
    private let clearSessionDetailsUseCase: ClearSessionDetailsUseCase
    private let obtainPincodeCreationParametersUseCase: ObtainPincodeCreationParametersUseCase
    
    init(view: BasicAuthorizationView,
         state: BasicAuthorizationPresenterState,
         basicAuthorizationUseCase: BasicAuthorizationUseCase,
         requestSMSConfirmationUseCase: RequestSMSConfirmationUseCase,
         clearSessionDetailsUseCase: ClearSessionDetailsUseCase,
         obtainPincodeCreationParametersUseCase: ObtainPincodeCreationParametersUseCase) {
    
        self.view = view
        self.state = state
        self.basicAuthorizationUseCase = basicAuthorizationUseCase
        self.requestSMSConfirmationUseCase = requestSMSConfirmationUseCase
        self.clearSessionDetailsUseCase = clearSessionDetailsUseCase
        self.obtainPincodeCreationParametersUseCase = obtainPincodeCreationParametersUseCase
    }
}

// MARK: - BasicAuthorizationPresenter

extension BasicAuthorizationPresenterImp: BasicAuthorizationPresenter {
    
    func didTapLogInButton(with username: String, and password: String) {
        state.enteredUsername = username
        
        view?.startIndication()
        basicAuthorizationUseCase.logIn(with: username, and: password)
        .then {
            self.obtainPincodeCreationParametersUseCase.obtainParameters()
        }.then { parameters in
            self.view?.onObtainPincodeParameters?(self.state.enteredUsername, parameters)
        }.catch { error in
            self.errorHandler?.proceed(error: error)
        }
    }
    
    func didTriggerViewReadyEvent() {
        view?.setupInitialState()
    }
    
    func didTriggerViewWillAppearEvent() {
        errorHandler?.configure(with: view)
    }
}

// MARK: - SMSConfirmationModuleOutput

extension BasicAuthorizationPresenterImp: SMSConfirmationModuleOutput {
    
    func didCancelConfirmationFlow() {
        clearSessionDetailsUseCase.clearSessionDetails()
        view?.stopIndication()
    } 
    
    func didFinishConfirmation() {
        view?.startIndication()
        obtainPincodeCreationParametersUseCase.obtainParameters()
        .then { parameters in
            self.view?.onObtainPincodeParameters?(self.state.enteredUsername, parameters)
        }.always {
            self.view?.stopIndication()
        }.catch { error in
            self.errorHandler?.proceed(error: error)
        }
    }
}
